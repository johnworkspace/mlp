function [ errorVal ] = iteracionValidacion(vectorValP,vectorValA,vectorMLP,f)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
global W
global B
clear A
clear N

errorVal = 0; 
for i = 1:length(vectorValP)

    
    [A,N] = propagacionHaciaAdelante(vectorMLP,vectorValP(i),f,W,B);
    
    errorVal = errorVal + (vectorValA(i)-A{length(vectorMLP)})*(vectorValA(i)-A{length(vectorMLP)});
    
    
    

end

errorVal = errorVal/length(vectorValP);

end

