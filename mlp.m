clear
fprintf('\nPerceptr�n Multicapa\n');
arqMLP= input('\nIntroduce la arquitectura del MLP en el formato nc1-nc2-nc3-...-ncm \n','s');
vectorMLP = obtenerVectorMLP(arqMLP);

for i=1:(length(vectorMLP)-1)
   
    fprintf('\nIntroduce la funci�n de activaci�n para la capa %d\n',i);
    
    f(i) = input('1.Lineal, 2.Sigmoidal: ');
    
end


p=zeros(vectorMLP(1),1);
A{1}=p;

for i=1:(length(vectorMLP)-1)
    
    %Construimos las matrices de pesos, bias y obtenemos los vectores a
    %evaluar en las funciones y su evaluaci�n correspondiente
    
    W{i} = rand(vectorMLP(i+1),vectorMLP(i));
    B{i} = rand(vectorMLP(i+1),1);
    V{i} = W{i}*A{i}+B{i};
    
    switch f(i)
       
        case 1
            
            A{i+1} = purelin(V{i});
            
        case 2
            
            A{i+1} = logsig(V{i});
    end   
    
    
end
fprintf('\nLa salida de la red neuronal es:\n')
disp(A{length(vectorMLP)});