function varargout = perceptronMC(varargin)
% PERCEPTRONMC MATLAB code for perceptronMC.fig
%      PERCEPTRONMC, by itself, creates a new PERCEPTRONMC or raises the existing
%      singleton*.
%
%      H = PERCEPTRONMC returns the handle to a new PERCEPTRONMC or the handle to
%      the existing singleton*.
%
%      PERCEPTRONMC('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in PERCEPTRONMC.M with the given input arguments.
%
%      PERCEPTRONMC('Property','Value',...) creates a new PERCEPTRONMC or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before perceptronMC_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to perceptronMC_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help perceptronMC

% Last Modified by GUIDE v2.5 15-Apr-2013 21:03:32

% Begin initialization code - DO NOT EDIT
global f;
global entradas;
global vectorMPL;
global pEnt
global pVal
global pPru
global gamma
global itmax
global itval
global eval
global vectorEntP
global vectorEntA
global vectorValP 
global vectorValA
global vectorPruP
global vectorPruA
global p
global aTarget



gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @perceptronMC_OpeningFcn, ...
                   'gui_OutputFcn',  @perceptronMC_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before perceptronMC is made visible.
function perceptronMC_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to perceptronMC (see VARARGIN)

% Choose default command line output for perceptronMC
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes perceptronMC wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = perceptronMC_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1
numFuncion = get(hObject,'Value');
numCapa = get(handles.popupmenu2,'Value');

global f
f(numCapa) = numFuncion



% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function arqText_Callback(hObject, eventdata, handles)
% hObject    handle to arqText (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of arqText as text
%        str2double(get(hObject,'String')) returns contents of arqText as a double
global arqMLP;
arqMLP = get(hObject,'String');


% --- Executes during object creation, after setting all properties.
function arqText_CreateFcn(hObject, eventdata, handles)
% hObject    handle to arqText (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu2.
function popupmenu2_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu2
numCapa = get(hObject,'Value');
global f
set(handles.popupmenu1,'Value',f(numCapa));


% --- Executes during object creation, after setting all properties.
function popupmenu2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




% --- Executes during object creation, after setting all properties.
function pushbutton2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over pushbutton2.
function pushbutton2_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

clear global
global vectorMLP
global f


arqMLP = get(handles.arqText,'String');
vectorMLP = obtenerVectorMLP(arqMLP);
for i=1 : length(vectorMLP)-1
   var{i} = int2str(i);    
end    
set(handles.popupmenu2,'String',var);
f = ones(1,length(vectorMLP)-1);
    
    


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global gamma
global itmax
global itval
global eval
global epsilon
global vectorEntP
global vectorEntA
global vectorValP 
global vectorValA
global vectorPruP
global vectorPruA
global vectorMLP
global f
global p
global aTarget


gamma = str2num(get(handles.gamma,'String'));
epsilon =str2num( get(handles.epsilon,'String'));
itmax = str2num(get(handles.itMax,'String'));
itval = str2num(get(handles.itVal,'String'));
eval = str2num(get(handles.Eval,'String'));
[W,B,erroresVal,erroresA]=entrenarRed(epsilon,gamma,itmax, itval,eval,vectorEntP,vectorEntA,vectorValP, vectorValA,vectorMLP,f);
vectorSalidaP = pruebas(vectorPruP, vectorPruA,f,W,B,vectorMLP);


temp=0;

for i=1:length(erroresVal)

    temp = temp + itval;
    
    erroresValT(i) = temp; 
    
    
end

temp=1;
for i=1:length(erroresA)

   
    
    if mod(temp,itval)==0
        
        temp = temp +1;
        erroresAT(i) = temp;
        
        
    else
        
        erroresAT(i) = temp;         
        
    end  
    
     temp = temp + 1;
    
end

axes(handles.axes2); 
cla; 
plot(erroresAT,erroresA,'r');
hold on;
plot(erroresValT,erroresVal,'b*');
legend('Error de aprendizaje','Error de validaci�n')


aTarget = aTarget';
p = p';
axes(handles.axes1); 
cla; 
plot(p,aTarget,'r');
hold on;
plot(vectorPruP,vectorSalidaP,'b*');
legend('Se�al original','Se�al MLP')
fid = fopen('bias.txt','wt');

for i=1:length(B)
    
    fprintf(fid,'B%d:\n',i);
    fprintf(fid, '%f\n',B{i});
end
fclose(fid);

fid = fopen('pesos.txt','wt');



for i=1:length(W)
    
    fprintf(fid,'W%d:\n',i);
    fprintf(fid, '%f\n',W{i});
end

fclose(fid);
    

% --- Executes on button press in checkbox1.
function checkbox1_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox1


% --- Executes on button press in checkbox2.
function checkbox2_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox2


% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global entradas
global pEnt
global pVal
global pPru
global vectorEntP
global vectorEntA
global vectorValP 
global vectorValA
global vectorPruP
global vectorPruA
global p
global aTarget

pEnt = str2num(get(handles.pEnt,'String'));
pVal = str2num(get(handles.pVal,'String'));
pPru = str2num(get(handles.pPru,'String'));
archivo=uigetfile({'*.csv'},'Abrir archivo de entradas');
entradas = load(archivo);
tamArchivo = length(entradas);
cont = 1;
cont2 = 1;
for i=1:length(entradas)

    if ( mod(i,2)) ==0
        
        aTarget(cont) = entradas(i);
        cont = cont +1;
        
    else
        
        p(cont2) = entradas(i);
        cont2 =cont2 +1;
    end
    
end

%Obtenemos vectores de entrenamiento, de validaci�n y de prueba
porcentajeEnt = pEnt * length(p)/100;
porcentajeVal = pVal * length(p)/100;
porcentajePru = pPru * length(p)/100;


tempP = p;
tempTarget = aTarget;

%Obtenemos vectores de entrenamiento
for i=1:porcentajeEnt
    
    indice = (round(mod((rand*length(tempP)),length(tempP))));
    
    if indice == 0
       
        indice = 1;
        
    end
    
    if indice > length(tempP)
       
        indice = length(tempP);
        
    end
    
    vectorEntP(i) = tempP(indice);
    vectorEntA(i) = tempTarget(indice);
    tempP(indice) = [];
    tempTarget(indice) = [];
    
end
vectorEntP;
%Obtenemos vectores de validacion

for i=1:porcentajeVal
    
    indice = (round(mod((rand*length(tempP)),length(tempP))));
     
     if indice == 0
       
        indice = 1;
        
    end
    
    if indice > length(tempP)
       
        indice = length(tempP);
        
    end
    
    vectorValP(i) = tempP(indice);
    vectorValA(i) = tempTarget(indice);
    tempP(indice) = [];
    tempTarget(indice) = [];
    
end
vectorValP;
%Obtenemos vectores de prueba

vectorPruP = tempP;
vectorPruA = tempTarget;

%
aTarget = aTarget';
p = p';
axes(handles.axes1); 
cla; 
plot(p,aTarget,'r');
legend('Se�al original','Se�al MLP')


function pEnt_Callback(hObject, eventdata, handles)
% hObject    handle to pEnt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of pEnt as text
%        str2double(get(hObject,'String')) returns contents of pEnt as a double
global pEnt;
pEnt = get(hObject,'String');


% --- Executes during object creation, after setting all properties.
function pEnt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pEnt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function pVal_Callback(hObject, eventdata, handles)
% hObject    handle to pVal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of pVal as text
%        str2double(get(hObject,'String')) returns contents of pVal as a double
global pVal;
pVal = get(hObject,'String');

% --- Executes during object creation, after setting all properties.
function pVal_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pVal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function pPru_Callback(hObject, eventdata, handles)
% hObject    handle to pPru (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of pPru as text
%        str2double(get(hObject,'String')) returns contents of pPru as a double
global pPru;
pPru = get(hObject,'String');

% --- Executes during object creation, after setting all properties.
function pPru_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pPru (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function gamma_Callback(hObject, eventdata, handles)
% hObject    handle to gamma (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of gamma as text
%        str2double(get(hObject,'String')) returns contents of gamma as a double
global gamma;
gamma = get(hObject,'String');

% --- Executes during object creation, after setting all properties.
function gamma_CreateFcn(hObject, eventdata, handles)
% hObject    handle to gamma (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function epsilon_Callback(hObject, eventdata, handles)
% hObject    handle to epsilon (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of epsilon as text
%        str2double(get(hObject,'String')) returns contents of epsilon as a double
global epsilon;
epsilon = get(hObject,'String');

% --- Executes during object creation, after setting all properties.
function epsilon_CreateFcn(hObject, eventdata, handles)
% hObject    handle to epsilon (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function itMax_Callback(hObject, eventdata, handles)
% hObject    handle to itMax (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of itMax as text
%        str2double(get(hObject,'String')) returns contents of itMax as a double
global itmax;
itmax = get(hObject,'String');

% --- Executes during object creation, after setting all properties.
function itMax_CreateFcn(hObject, eventdata, handles)
% hObject    handle to itMax (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function itVal_Callback(hObject, eventdata, handles)
% hObject    handle to itVal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of itVal as text
%        str2double(get(hObject,'String')) returns contents of itVal as a double
global itval;
itval = get(hObject,'String');

% --- Executes during object creation, after setting all properties.
function itVal_CreateFcn(hObject, eventdata, handles)
% hObject    handle to itVal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Eval_Callback(hObject, eventdata, handles)
% hObject    handle to Eval (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Eval as text
%        str2double(get(hObject,'String')) returns contents of Eval as a double
global eval;
eval = get(hObject,'String');

% --- Executes during object creation, after setting all properties.
function Eval_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Eval (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
