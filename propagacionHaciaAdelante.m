function [ A,N ] = propagacionHaciaAdelante(vectorMLP,p,f,W,B)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
clear A
clear N

A{1}=p;

for i=1:(length(vectorMLP)-1)
    
 
  
    N{i} = W{i}*A{i}+B{i};
    
    switch f(i)
       
        case 2
            
            A{i+1} = purelin(N{i});
            
        case 1
            
            A{i+1} = logsig(N{i});
            
        case 3
            
            A{i+1} = tansig(N{i});
    end   
    
end

end


