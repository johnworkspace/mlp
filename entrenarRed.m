function [ W,B,erroresVal,erroresA ] = entrenarRed(epsilon,gamma,itmax, itval,eval,vectorEntP,vectorEntA,vectorValP, vectorValA,vectorMLP,f)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here


%Construimos las matrices de pesos y bias 
global W
global B
Waux=[];
Baux=[];

erroresVal(1) =0;

for i=1:(length(vectorMLP)-1)
    
    W{i} = rand(vectorMLP(i+1),vectorMLP(i));
    B{i} = rand(vectorMLP(i+1),1);


end

contErrores = 1;
contErroresItA =1;
incrementos =0;


for i=1:itmax

   
    if (mod(i,itval)) == 0
    
        erroresVal(contErrores) = iteracionValidacion(vectorValP,vectorValA,vectorMLP,f);
        contErrores = contErrores +1;
        incremento = calcularIncrementos(erroresVal);
       
        if incremento
           
            incrementos = incrementos +1;
            
        else
            
            incrementos = 0;
            
            Waux = W;
            Baux = B;
            
        end
        
        
    else
        
        
      [termino,errorIteracion] = iteracionAprendizaje(vectorEntP,vectorEntA,vectorMLP,epsilon,gamma,f);
      erroresA(contErroresItA) = errorIteracion;
      contErroresItA = contErroresItA + 1; 
      
    
      
    end
    
    if incrementos == eval
        
        W = Waux;
        B = Baux;
        
        break;
    end
    
    if termino
       
        
        break;
        
        
    end
    i

end

end


