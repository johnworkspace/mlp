function [ termino,errorIteracion ] =  iteracionAprendizaje(vectorEntP,vectorEntA,vectorMLP,epsilon,gamma,f)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
    
global W
global B
clear errorIteracion
clear A
clear N
clear F
clear matrizAux

errorIteracion = 0;

    for i=1:length(vectorEntP)
       
        
       [A,N] = propagacionHaciaAdelante(vectorMLP,vectorEntP(i),f,W,B);       
       a = A{length(vectorMLP)};
       errorIteracion = errorIteracion + ((vectorEntA(i)-a)*(vectorEntA(i)-a));
       %Calculamos las matrices F
       for j=2:length(vectorMLP)
           matrizAux =[];
           neuronas = N{j-1};
           
           for k=1:length(neuronas(:,1))
           
                
               switch f(j-1) 
               
                   case 2
                       
                    matrizAux(k,k) = 1;
           
                
                   case 1
                
                    matrizAux(k,k) = logsig(neuronas(k))*(1-logsig(neuronas(k)));
                    
                   case 3
                       
                    matrizAux(k,k) = 1- (tansig(k)*tansig(k));
                       
                
               end               
                
           
           end
           F{j-1} = matrizAux;        
       end
       
       
       %Calculamos las sensitividades
       
       S{length(vectorMLP)-1} = -2*F{length(vectorMLP)-1}*(vectorEntA(i)-a);
      
       for j = length(vectorMLP)-2:-1:1
           
           
           S{j} = F{j}*W{j+1}'*S{j+1};
           
       end
       
       %Obtenemos las reglas de aprendizaje
       
       for j = 1:length(vectorMLP)-1
          
           
           W{j} = W{j} - gamma*S{j}*A{j}';
           B{j} = B{j} - gamma*S{j};
           
           
       end             
        
    end
    
    errorIteracion = errorIteracion/length(vectorEntP);
    
    
    if errorIteracion < epsilon || errorIteracion ==0
        
        termino = true;
       
    else
        
        termino = false;
        
    end

end

